package INF102.lab1.triplicate;

import java.util.List;
import java.util.Collections;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        // Sort the list 
        Collections.sort(list, Collections.reverseOrder());
        // Iterate through list and check if the next two elements are equal and return the element of type T if they are
        for (int i = 0; i < list.size() - 2; i++) {
            if (list.get(i).equals(list.get(i + 1)) && list.get(i).equals(list.get(i + 2))) {
                return list.get(i);
            }
        }
        // If no triplicate is found, return null
        return null;
         
            
            

    }
    
}
